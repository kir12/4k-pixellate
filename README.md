# 4K-Pixellate
>Touhou's Waifu 4K Converter

A small program that utilizes facial recognition to snapshot a face on a picture, then stretches that face to 4K proportions. 

![Marisa Kirisame](marisa.png)

## Running 4K-Pixellate

### Regarding Photos

NOTE: 4K-Pixellate works (mostly) best with artwork created by ZUN. Fanart is also supported, but results are more varied. The facial-recognition package implemented (specifically `face-anime`) is geared towards anime faces, so anime characters should work. (Results are varied) The inserted photo should be composed of **just one person facing forward**

### End-Users

Download the latest executable file for your OS from `releases/`. (NOTE: at this time, only Linux is supported, but Windows and MacOS will be added very soon)
Double-click the executable to run the program.

### Developers

This procedure assumes a user has a working setup of Python 3 and pip. (A Linux setup is preferable) 

1. `git clone https://gitlab.com/asi14/4k-pixellate.git` in a directory of your choice.
2. If `virtualenv` is not installed, run `pip install virtualenv`.
3. Create a new virtual environment with `python -m virtualenv myenv` (or `py -m` for Windows) in a directory of your choice. Activate `myenv` via `source /path/to/emyenv/bin/activate`.
4. `cd` into `4k-pixellate` and install dependencies via `pip install -r requirements.txt`
5. `cd` into `src/` and run `python start.py`. The program should start from there.

Depending on your operating system setup, a few dependencies may come up as necessary. Install them as necessary. If you would like to compile your own executables, run the below commands, depending on your OS.

-Linux: `pyinstaller --distpath ../bin/linux/dist/ --clean --specpath ../bin/linux/ -p ~/virtual_environments/pixellate/lib/python3.6/ --workpath ../bin/linux/build/ --onefile pixel_main.py userWindow.py`

## Inspiration

### What is Touhou?

The [Touhou Project](https://en.touhouwiki.net/wiki/Touhou_Project) is series of Japanese bullet-hell video games (also called **danmaku**) created by a one-man-team [Team Shanghai Alice](https://en.touhouwiki.net/wiki/Team_Shanghai_Alice) led by game creator ZUN. (See Acknowledgements) Touhou has spanned nearly 20 years and has since expanded to include its own series of manga, as well as a plethora of fan-made doujin + games. (As well as several anime) It has several popular subreddits, the most popular being [/r/touhou](https://reddit.com/r/touhou).

### What does Touhou have to do with any of this?

Inspiration for this project began with [/r/touhoujerk](https://reddit.com/r/touhoujerk)'s background photo, specifically [Sanae Kochiya](https://i.imgur.com/90XbV8X.jpg) from the video game [Touhou Kanjuden ~ Legacy of Lunatic Kingdom](https://en.touhouwiki.net/wiki/Legacy_of_Lunatic_Kingdom). This same image was re-distributed on a similar subreddit [/r/touhoubackgrounds](https://reddit.com/r/TouhouWallpaper/), except expanded to 4K proportions as a desktop background. The image's clarity was of course not conserved, rendering the end product [comically pixellated](https://imgur.com/FScKKWE). Shortly after discovering this masterpiece, I hand-crafted a similar version for another character from the Touhou Project, specifically the deuteragonist [Marisa Kirisame](https://www.reddit.com/r/TouhouWallpaper/comments/8m6m9y/3840x2160_marisa_kirisame/) from the game [Touhou Tenkuushou ~ Hidden Star in Four Seasons](https://en.touhouwiki.net/wiki/Hidden_Star_in_Four_Seasons).

The process I went about making my masterpiece, though, was fairly time-consuming. I had to find a decent picture of a Touhou character, take a screenshot of her<sup>1</sup> face, open Gimp, expand that screenshot to 4K proportions, then upload the picture to a website that pixellated the image. That's a lot of work! My next thought was to write a script that automatically did all those things. [Redditisai 2018](https://www.reddit.com/r/touhou/comments/8qp93t/redditaisai_2018_last_minute_signup/)'s being right around the corner further motivated this project, especially since this project would be an especially unique contribution to the virtual convention. Hence, this ... thing.

## Contributing

If you'd like to contribute to this project, feel free to submit an Issue or Pull Request. Please be aware that this repo is hosted on Gitlab, so procedures for doing either of those things may differ from Github.

## Acknowledgements / Disclaimers

Credits for the facial recognition portion of this application goes to the [`face_recognition`](https://github.com/ageitgey/face_recognition) and [`face-anime`](https://github.com/nagadomi/lbpcascade_animeface) Python packages, written by [Adam Geitgey](https://github.com/ageitgey) and [nagadomi](https://github.com/nagadomi) respectively.  The Touhou Project itself is accreddited to its creator, [ZUN](https://en.touhouwiki.net/wiki/ZUN). Credit for the original masterpiece of Sanae that inspired it all goes to the reddit user [/u/Fauxm](https://www.reddit.com/user/Fauxm). (An unknown reddit user who has since deleted his/her account made the Desktop version) Credits for this project's mascot (A Walfas version of Marisa) goes to the DeviantArt user [rsgmaker](https://www.deviantart.com/rsgmaker). All of these people's various projects/contributions provided integral foundations for this project, and absent any of them this project would not exist. It is strongly encouraged to visit each of these users' individual projects.

Please consult `LICENSE` for more information about liability/disclaimers.

