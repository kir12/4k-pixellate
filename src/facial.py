from PIL import Image
#import face_recognition
import animeface

#converts any image not JPG to JPG so facial recognition can process it
def convert_to_jpg(image):
	if(image.format not in ['JPG']):
		image =image.convert('RGB')
	return image 
'''
#given an image url, converts image to JPG if necessary and extracts face capture
def face_capture(image_url):

	#takes image and extracts coordinates of face
	convert_to_jpg(image_url)
	image = face_recognition.load_image_file(image_url)
	face_locations = face_recognition.face_locations(image)

	#per each face, puts toether the face
	for face_location in face_locations:
		top,right,bottom,left=face_location
		face_image=image[top:bottom,left:right]
		pil_image=Image.fromarray(face_image)
		print('test')
		pil_image.show()
'''
#given an image url, (ANIME FACE) converts to JPG and extracts face capture
#ASSUMES ONLY ONE FACE
def face_anime(image_url):
	#opens image and converts to JPG as necessary
	image = Image.open(image_url)
	image = convert_to_jpg(image)

	#takes image and extracts coordiante of face
	try:
		faces = animeface.detect(image)
		fp = faces[0].face.pos
		
		#takes the face and assembes the final screenshot
		crop_img = image.crop((fp.x, fp.y, fp.x+fp.width, fp.y+fp.height)) #specs: x, y, x + width, y + width
		return crop_img

	#handles images that don't pass the facial recgnition test by ignoring it
	#might use regular face_recogniion as backup plan
	except IndexError:
		print('WARNING: The image ' + str(image_url) + ' was unsuccessfully processed')

	
