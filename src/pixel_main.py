import userWindow
from userWindow import *
from tkinter import *
from tkinter.ttk import *
import sys

if __name__=='__main__':

#the handler methods for bulk file input and single file input respectively
#check if resized image result isn't None, and also handles saving
#program gracefully exits here
	def bulk_handler():
		images_and_names = bulk_get()
		if images_and_names is not None:
			bulk_save(images_and_names)
		sys.exit()

	def single_handler():
		image = get_image()
		if image is not None:
			save_image(image)
		sys.exit()

#tkinter declares the dialog's window frame
	window = Tk()
	window.title("Touhou 4K Waifu Converter")

	def on_closing():
		window.destroy()
		sys.exit()

	window.protocol('WM_DELETE_WINDOW',on_closing)

#creates radio buttons
#selected: contains the index of user choice (starting from 1) linked to variable
#value: the index
#text: user output
	selected = StringVar(master=window)
	rad1 = Radiobutton(window,text='Bulk Image Import', value=1, variable=selected)
	rad2 = Radiobutton(window,text='Single Import', value=2, variable=selected)

#closes initil dialog, gets user input, and runs appropriate method
	def clicked():
		if selected.get()=='':
			pass
		else:
			window.destroy()
			options = ['Bulk Image Import','Single Import']
			selection = int(selected.get())-1
			if selection==0:
				bulk_handler()
			elif selection==1:
				single_handler()

#button that when clicked runs clicked()
	btn = Button(window, text="Click Me", command=clicked)

#visual arrangements
	rad1.grid(column=0, row=0)
	rad2.grid(column=1, row=0)
	btn.grid(column=2, row=0)

#opens the window
	window.mainloop()

