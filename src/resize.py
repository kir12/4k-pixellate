from PIL import Image
from facial import *

#resizes images to 4K specs
#seems to only work with ZUN's images only
def resizeImage(image_url):
	face_image = face_anime(image_url)
	width=3840
	height=2160
	if face_image is None:
		return None
	else:
		resized_image = face_image.resize((width,height))
		return resized_image
