import tkinter as tk
import os
from tkinter import Label,Entry,mainloop,Frame,Button,W
import requests
from PIL import Image
from tkinter import filedialog
from resize import *
from io import BytesIO


#root stuff
root = tk.Tk()
root.withdraw()

#gets a directory full of images, parses it for images only, creates a list full of resized images and returns it along with its original names
def bulk_get():

	#create resized image list and get directory of images
	resized_images=[]
	original_names=[]
	folder_path=filedialog.askdirectory(title="Select Directory for Bulk Image Import")
	if len(folder_path)==0:
		return None
	else:
		#go through only images and runs resized_images on them
		for img_file in os.listdir(folder_path):
			
			#gets file extension & appends names to original_names
			extension=(os.path.splitext(img_file)[1]).lower()
			original_names.append(os.path.splitext(img_file)[0])

			#checks if file is valid image nd runs resized_images on it
			if extension in ['.jpg','.jpeg','.png','.gif','.tga']:
				print(os.path.join(folder_path,img_file))
				resized_images.append(resizeImage(os.path.join(folder_path,img_file)))
	
	#returns everything as a list to be processed later
	return [resized_images,original_names]

#using the list outputted from bulk_get(), saves each image using a user-inputted general directory and the same original image names
def bulk_save(images_and_names):

	#declares arrays to be used
	resized_images = images_and_names[0]
	original_names=images_and_names[1]
	
	#gets save path for all images
	save_path = filedialog.askdirectory(title="Select Directory where to Save Bulk Images")
	if len(save_path)==0:
		pass
	else:

		#used index-based for loop so that resized_images and original_names can be used at once
		for i in range(len(original_names)):
			
			#by checking if an image didn't actually pass the face recognition and ignoring it, this bypasses index-related erors
			if resized_images[i] is None:
				pass
			else:

				#puts togethr a saved image's final path, saves everything, and exits
				specific_path=os.path.join(save_path,original_names[i]+'.jpg')
				resized_images[i].save(specific_path)
				resized_images[i].close()

#given a certain image path, gets its resized image (4K) and returns it back out
def get_image():

	#ask filename
	file_path = filedialog.askopenfilename(title="Select Individual Image")
	if len(file_path)==0:
		return None
	else:
		
		#get resized image
		resized_image=resizeImage(file_path)
		return resized_image

#given  resized image, saves it
def save_image(image):

	#asks for final name of image
	file_path=filedialog.asksaveasfilename(defaultextension=".jpg",title="Save Image")	
	if len(file_path)==0:
		return None
	else:
		#saves it
		image.save(file_path)
		image.close()
		Image.open(file_path).show() #note: shows from /tmp not saved image
